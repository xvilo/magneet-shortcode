<?php
/**
 * @package magneet_shortcode
 * @version 1.0
 */
/*
Plugin Name: Magneet Shortcode
Plugin URI: https://magneetonline.nl/
Description: Voegt eigen shortcodes toe. Zoals <code>[featured link="http://urlvansite.nl/url" img="http://urlnaarafbeeld.ing" title="De titel" content="Inhoud onder het streepje"]</code>
Author: Sem Schilder
Version: 1.0
Author URI: http://sem.design/
*/

add_action('wp_head','hook_css');

function hook_css() {

	$output="<style>
				.preview *{-webkit-transition: all .2s ease;transition: all .2s ease;}
				.preview .container{ width: 100%; background-repeat: no-repeat; -webkit-background-size: cover!important; -moz-background-size: cover!important; -o-background-size: cover!important; background-size: cover!important;  margin-top:20px;
				}	
				.preview .container .outer {width: 100%;padding-top: 45%; position: relative; display: block;}
				.preview.long .container .outer {padding-top: 147%;}
				.preview.rect .container .outer {padding-top: 95%;}
				.preview.long .container .outer .inner {}
				.preview .container .outer .inner {position: absolute; display:block; top: 0;left: 0;right: 0;bottom: 0; overflow: hidden; margin: 15px; opacity: 0;}
				.preview .container .outer .inner h3 {font-size: 20px;color: white;text-align: center;font-weight: 400;margin-top:7px; margin-bottom: 0px; white-space: nowrap;overflow: hidden; text-overflow: ellipsis;}
				.preview .container .outer .inner .short {margin: 7px auto 7px auto;}
				.preview .container .outer .inner p {font-size: 12px; color:white;/* white-space: nowrap;overflow: hidden; text-overflow: ellipsis; */ text-align: center; margin: 12px;}
				
				.preview .container .outer .inner {opacity: 0;}
				.preview:hover .container .outer .inner {opacity: 1;}
				.preview .container .outer {background: rgba(208, 112, 137,0);}
				.preview:hover .container .outer {background: rgba(208, 112, 137,0.58)}
				.short{
					width: 20%;
				    height: 1px;
				    background: white;
				    display: block;
				    margin-top: 25px;
				}
				/* adjustments for mobile */
				@media (max-width: 767px){
					.preview .container .outer .inner {opacity: 1;}
					.preview .container .outer {background: rgba(208, 112, 137,0.58)}
				}
			</style>";
	echo $output;

}

function featured_func( $atts ) {
    $a = shortcode_atts( array(
        'link' => '#',
        'img' => 'http://placehold.it/800?text=afbeelding+hier+plaatsen!',
        'title' => 'De titel',
        'content' => 'De inhoud',
        'size' => ''
    ), $atts );
	
	return "<div class='preview {$a['size']}'>
		<div class='container' style='background: url({$a['img']});'>
		    <a href='{$a['link']}'>
			    <span class='outer'>
			        <span class='inner'>
			        	<span class='center'>
				        	<h3>{$a['title']}</h3>
				        	<div class='short'></div>
				        	<p>{$a['content']}</p>
			        	</span>
			        </span>
			    </span>
		    </a>
		</div>
	</div>";
    //return "title = {$recent_posts[$cycle]['post_title']} content = {$recent_posts[$cycle]['post_content']} url = {$recent_posts[$cycle]['guid']} img = {$image[0]}";
}
add_shortcode( 'featured', 'featured_func' );


?>
